﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace PushDeer_CSharp_SDK
{
    /// <summary>
    /// PushDeer发送消息客户端
    /// </summary>
    public class PushDeerClient
    {
        // 默认使用官方地址
        private string host = "https://api2.pushdeer.com";

        private string path = "/message/push";

        private string pushKey;

        /// <summary>
        /// 设置自己搭建的pushdeer服务器
        /// </summary>
        /// <param name="selfHost"></param>
        public void SetSelfHost(string selfHost)
        {
            host = selfHost;
        }

        /// <summary>
        /// 设置自己发送消息的pushkey, 需要提前生成
        /// </summary>
        /// <param name="key"></param>
        public void SetPushKey(string key)
        {
            pushKey = key;
        }

        /// <summary>
        /// 发送文字消息
        /// </summary>
        /// <param name="textContent">文字内容</param>
        /// <param name="description">文字描述</param>
        public bool SendTextMessage(string textContent, string description="")
        {
            return SendMessage(pushKey, "text", description, textContent);
        }

        /// <summary>
        /// 发送图片消息
        /// </summary>
        /// <param name="imageUrl">图片的地址</param>
        public bool SendImageMessage(string imageUrl)
        {
            return SendMessage(pushKey, "image", "", imageUrl);
        }

        /// <summary>
        /// 发送markdown消息
        /// </summary>
        /// <param name="title">markdown标题</param>
        /// <param name="markDownContent">markdown内容</param>
        public bool SendMarkDownMessage(string title, string markDownContent)
        {
            return SendMessage(pushKey, "markdown", markDownContent, title);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="type"></param>
        /// <param name="desp"></param>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private bool SendMessage(string key, string type, string desp, string text)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentException("Push key cannot be empty");
            }

            string url = host + path + "?pushkey=" + key + "&text=" + text + "&desp=" + desp + "&type=" + type;
            Debug.Print("send message url: " + url);
            string res = HttpTools.GetRequest(Uri.EscapeDataString(url));
            if (string.IsNullOrEmpty(res))
            {
                return false;
            }
            Debug.Print("send message response: "+ res);
            var response = JsonConvert.DeserializeObject<MessageResponse>(res);
            return  response != null && response.Code == 0;
        }
    }

    /// <summary>
    /// 发送请求工具类
    /// </summary>
    public class HttpTools
    {
        /// <summary>
        /// 发送get请求
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <returns>返回响应的字符串</returns>
        public static string GetRequest(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "get";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (StreamReader sr = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException(), Encoding.UTF8))
            {
                string result = sr.ReadToEnd();
                return result;
            }
        }
    }

    /// <summary>
    /// 发送消息响应
    /// </summary>
    public class MessageResponse
    {
        /// <summary>
        /// 状态码 0 成功 非0 失败
        /// </summary>
        public Int32? Code { get; set; }
    }
}
