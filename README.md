# PushDeer_CSharp_SDK

#### 简介

C# SDK for Pushdeer，For more detail, please see : https://github.com/easychen/pushdeer

支持配置Pushdeer的服务器地址和pushkey,从而发送文字、图片或者markdown消息。

#### 使用说明

这是一个C# 类库项目

使用Visual Studio 2019打开解决方案，点击菜单"生成"-"生成解决方案"即可生成dll文件。

默认生成的dll文件在根目录的bin/Debug/文件夹会生成一个PushDeer_CSharp_SDK.dll的文件。

如果不想生成，直接使用我提交的dll文件试试。

新建一个控制台程序，在左侧依赖项单击右键添加项目引用， 选择浏览之后选择本地的dll文件。

还需要引入json依赖。使用NuGet程序包管理器安装Newtonsoft.Json这个依赖。

在Program.cs文件：
```
class Program
    {
        static void Main(string[] args)
        {
            PushDeerClient pushDeerClient = new PushDeerClient();
            pushDeerClient.SetPushKey("your_push_key");
            pushDeerClient.SendTextMessage("战争原理，世界和平");
            pushDeerClient.SendImageMessage("http://www.ruanyifeng.com/images_pub/pub_218.jpg");
            pushDeerClient.SendMarkDownMessage("测试markdown", "**加粗**");
        }
    }
```

点击运行之后在手机上查看是否能够正常接收到消息。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
